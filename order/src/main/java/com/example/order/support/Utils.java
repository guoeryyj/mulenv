package com.example.order.support;

import com.alibaba.ttl.TransmittableThreadLocal;

public class Utils {
    private static final TransmittableThreadLocal<String> versionLocal= new TransmittableThreadLocal();

    public static String GetVersion(){
        String version = versionLocal.get();
        return version == null?Constont.DEFAULT_VERSION:version;
    }

    public static void SetVersion(String version){
        versionLocal.set(version);
    }
    public static void CleanVersion(){
        versionLocal.remove();
    }
}
