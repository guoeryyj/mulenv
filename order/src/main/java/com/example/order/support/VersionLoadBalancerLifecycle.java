package com.example.order.support;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.*;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class VersionLoadBalancerLifecycle implements LoadBalancerLifecycle<RequestDataContext,Object,Object>
{
    @Override
    public void onStart(Request request) {
        Object context = request.getContext();
        if (context instanceof RequestDataContext) {
            RequestDataContext dataContext = (RequestDataContext) context;
            String version = Utils.GetVersion();
            dataContext.getClientRequest().getHeaders().add(Constont.VERSION,version);
        }
    }
    @Override
    public void onStartRequest(Request request, Response lbResponse) {
    }
    @Override
    public void onComplete(CompletionContext completionContext) {
    }
}
