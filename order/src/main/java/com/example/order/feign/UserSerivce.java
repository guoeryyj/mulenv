package com.example.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "user")
public interface UserSerivce {
    @GetMapping("/demo")
    String demo();
}

