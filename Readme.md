#多环境治理示例代码

##操作步骤
1. 打包
```mvn clean install -DskipTests```

2. 启动实例
```
nohup java -jar -Dserver.port=8761 eureka/target/eureka-0.0.1-SNAPSHOT.jar  >null 2>&1 &
nohup java -jar -Dserver.port=5000 gateway/target/gateway-0.0.1-SNAPSHOT.jar  >null 2>&1 &
nohup java -jar -Dserver.port=8001 order/target/order-0.0.1-SNAPSHOT.jar  >null 2>&1 &
nohup java -jar -Dversion=v1 -Dserver.port=8002 order/target/order-0.0.1-SNAPSHOT.jar  >null 2>&1 &
nohup java -jar -Dversion=v2 -Dserver.port=8003 order/target/order-0.0.1-SNAPSHOT.jar  >null 2>&1 &

nohup java -jar -Dserver.port=9001 user/target/user-0.0.1-SNAPSHOT.jar  >null 2>&1 &
nohup java -jar -Dversion=v1 -Dserver.port=9002 user/target/user-0.0.1-SNAPSHOT.jar  >null 2>&1 &
```
3. 验证
```
curl --location --request GET 'localhost:5000/order/demo'
```
返回 order:default/user:default

```
curl --location --request GET 'localhost:5000/order/demo' \
--header 'version: v1'
```
返回 order:v1/user:v1

```
curl --location --request GET 'localhost:5000/order/demo' \
--header 'version: v2'
```
返回 order:v2/user:default